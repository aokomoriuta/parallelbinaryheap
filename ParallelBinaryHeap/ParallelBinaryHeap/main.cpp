﻿#include <iostream>
#include <memory>
#include <algorithm>
#include <random>
#include <functional>

#include "BinaryHeap.hpp"
#include "ParallelBinaryHeap.hpp"
#include "ParallelBinaryHeapN.hpp"
#include "Timer.hpp"

#define TARGET_SIZE (1234)
int main()
{
	const std::size_t n = TARGET_SIZE * 100000;

	std::cout << "Extract first " << TARGET_SIZE << "th values from " << n << " values" << std::endl;
	std::cout << "...initializing test" << std::endl;
	std::unique_ptr<double[]> vals(new double[n]);

	const int seed = 713;
	std::mt19937 mt(seed);
	std::uniform_real_distribution<double> rand(0, 1000);
	std::generate_n(vals.get(), n, [&rand, &mt](){return rand(mt); });

	std::unique_ptr<double[]> sorted(new double[n]);
	std::copy_n(vals.get(), n, sorted.get());

	Timer timer;

	std::cout << "Binary heap            : " << std::flush;
	BinaryHeap heap(TARGET_SIZE);
	timer.Start();
	heap.Get(vals.get(), n);
	std::cout << timer.Time() << "[ms]" << std::endl;

#if 0
	std::cout << "Parallel binary heap   : " << std::flush;
	ParallelBinaryHeap<4> parallelHeap(TARGET_SIZE);
	timer.Start();
	parallelHeap.Get(vals.get(), n);
	std::cout << timer.Time() << "[ms]" << std::endl;
#endif

	std::cout << "Parallel N binary heap : " << std::flush;
	ParallelBinaryHeapN<8> parallelNHeap(TARGET_SIZE);
	timer.Start();
	parallelNHeap.Get(vals.get(), n);
	std::cout << timer.Time() << "[ms]" << std::endl;

#if 0
	std::cout << "Sort                   : " << std::flush;
	timer.Start();
	std::sort(sorted.get(), sorted.get() + n, std::greater<double>());
	std::cout << timer.Time() << "[ms]" << std::endl;
#endif

	std::cout << "...check result: " << std::flush;
	for (std::size_t i = 0; i < TARGET_SIZE; i++)
	{
#if 0
		// 並列実行した結果が違ったらエラー
		if (parallelHeap[i] != heap[i])
		{
			std::cout << std::endl
				<< "ParallelHeap[" << i << "] = " << parallelHeap[i] << " != " << std::endl
				<< "Heap        [" << i << "] = " << heap[i] << std::endl;
			return -1;
		}

		// ソートの結果になかったらエラー
		const auto p1 = std::find(sorted.get(), sorted.get() + TARGET_SIZE, heap[i]);
		if (p1 == sorted.get() + TARGET_SIZE)
		{
			std::cout << "Heap[" << i << "] = " << heap[i] << " is wrong!" << std::endl;
			return -2;
		}

		const auto p2 = std::find(sorted.get(), sorted.get() + TARGET_SIZE, parallelNHeap[i]);
		if (p2 == sorted.get() + TARGET_SIZE)
		{
			std::cout << "ParallelNHeap[" << i << "] = " << parallelNHeap[i] << " is wrong!" << std::endl;
			return -3;
		}
#endif
		if (parallelNHeap[i] != heap[i])
		{
			std::cout << std::endl
				<< "ParalleNlHeap[" << i << "] = " << parallelNHeap[i] << " != " << std::endl
				<< "Heap         [" << i << "] = " << heap[i] << std::endl;
			return -1;
		}
	}

	std::cout << "OK" << std::endl;

	return 0;
}
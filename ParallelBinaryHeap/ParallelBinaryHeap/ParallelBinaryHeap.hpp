﻿#ifndef PARALLELBINARYHEAP_HPP_INCLUDED
#define PARALLELBINARYHEAP_HPP_INCLUDED
#include <cstddef>
#include <memory>

#include "Semaphore.hpp"
#include "PooledThread.hpp"

template < std::size_t N_PARALLEL = 2>
class ParallelBinaryHeap
{
private:
	// 番号付けが面倒なので0は使わずに1を根にする
	static const std::size_t rootIndex = 1;

	// 最小二分ヒープ
	std::unique_ptr<double[]> heap;

	// ヒープの大きさ
	const std::size_t heapSize;

	// 根の直下の子を操作する場合に確保
	Semaphore rootChildren;

	// 根の子以下をdown-heapして揃えるスレッド
	PooledThread downChild[N_PARALLEL];

	void Up(const double val, const std::size_t nIn)
	{
		const std::size_t n = nIn + 1; // TODO: 必要ならn<=heapSizeの確認

		// 末端に追加
		heap[n] = val;

		// 根に到達するまで繰り返し
		double child = val;
		for (std::size_t childIndex = n; childIndex > rootIndex;)
		{
			// 子の方が小さかったら
			const std::size_t parentIndex = childIndex / 2;
			const double parent = heap[parentIndex];
			if (child < parent)
			{
				// 親と子を交換
				heap[parentIndex] = child;
				heap[childIndex] = parent;
			}
			else
			{
				// ヒープ条件を満たしているので完了
				break;
			}

			// 次は親を見る
			childIndex = parentIndex;
		}
	}

	void Down(const double val)
	{
		// 根（最小値）より大きかったら
		double root = heap[rootIndex];
		if (val > root)
		{
			// 交換
			heap[rootIndex] = val;
			root = val;

			// 根の直下を見る
			rootChildren.Acquire();

			// 子の選択
			std::size_t childIndex = 2;
			double child = heap[2]; // 左の子
			const double right = heap[3]; // 右の子（必ず存在する前提 TODO: ないなら処理を飛ばす）
			if (right < child)
			{
				// 右の子を選択
				child = right;
				childIndex = 3;
			}

			// 根の方が大きかったら
			if (root > child)
			{
				// 根と子を交換
				heap[rootIndex] = child;
				heap[childIndex] = root;

				// 子以下を揃えるスレッドを起動
				for (std::size_t i = 0; i < N_PARALLEL; i++)
				{
					const bool isOK = downChild[i].Start([this, root, childIndex]()
					{
						const double child = root;

						// 根の孫を見る
						std::size_t grandChildIndex = childIndex * 2; // 左の孫
						double grandChild = this->heap[grandChildIndex];
						const std::size_t rightIndex = grandChildIndex + 1;  // 右の孫（必ず存在する前提 TODO: ないなら処理を飛ばす）
						const double right = this->heap[rightIndex];

						if (right < grandChild)
						{
							// 右の子を選択
							grandChild = right;
							grandChildIndex = rightIndex;
						}

						// 子より孫の方が大きかったら
						if (child > grandChild)
						{
							// 子と孫を交換
							this->heap[childIndex] = grandChild;
							this->heap[grandChildIndex] = child;

							// 根の直下はもう触らないので解放
							this->rootChildren.Release();
						}
						else
						{
							// ヒープ条件を満たしているので、セマフォを解放して終了
							this->rootChildren.Release();
							return;
						}

						// 孫から末端に到達するまで繰り返し
						double parent = child;
						for (std::size_t parentIndex = grandChildIndex; parentIndex <= this->heapSize / 2;)
						{
							// 左の子
							std::size_t childIndex = parentIndex * 2;
							double child = this->heap[childIndex];

							// 右の子があって
							const std::size_t rightIndex = childIndex + 1;
							if (rightIndex <= this->heapSize)
							{
								// 右の子の方が小さかったら
								double right = this->heap[childIndex + 1]; // 右の子
								if (right < child)
								{
									// 右の子を選択
									child = right;
									childIndex = rightIndex;
								}
							}

							// 親の方が大きかったら
							if (parent > child)
							{
								// 親と子を交換
								this->heap[parentIndex] = child;
								this->heap[childIndex] = parent;
							}
							else
							{
								// ヒープ条件を満たしているので完了
								break;
							}

							// 次は子を見る
							parentIndex = childIndex;
						}
					});

					// 開始できたら完了
					if (isOK)
					{
						break;
					}
				}
			}
			else
			{
				// 根の方が大きかったら何もせずにセマフォ解放
				rootChildren.Release();
			}
		}
	}

public:
	ParallelBinaryHeap(const std::size_t targetSize) : 
		heap(new double[targetSize+1]),
		heapSize(targetSize)
	{}

	void Get(const double vals[], const std::size_t total)
	{
		// 対象番までのヒープを作成
		for (std::size_t i = 0; i < heapSize; i++)
		{
			Up(vals[i], i);
		}

		// 残りはヒープに追加
		for (std::size_t i = heapSize; i < total; i++)
		{
			Down(vals[i]);
		}
	}

	double operator[](const size_t i)
	{
		return heap[i + 1];
	}
};
#endif

﻿#ifndef TIME_HPP_INCLUDED
#define TIME_HPP_INCLUDED
#include <chrono>

class Timer
{
private:
	typedef std::chrono::time_point<std::chrono::system_clock> time_point;

	time_point begin;

public:
	void Start()
	{
		this->begin = std::chrono::system_clock::now();
	}

	long long Time()
	{
		const auto end = std::chrono::system_clock::now();
		return std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
	}
};
#endif

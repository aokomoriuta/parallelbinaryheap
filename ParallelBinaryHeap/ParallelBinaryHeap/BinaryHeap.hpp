﻿#ifndef BINARYHEAP_HPP_INCLUDED
#define BINARYHEAP_HPP_INCLUDED
#include <cstddef>
#include <memory>

class BinaryHeap
{
private:
	// 番号付けが面倒なので0は使わずに1を根にする
	static const std::size_t rootIndex = 1;

	// 最小二分ヒープ
	std::unique_ptr<double[]> heap;

	// ヒープの大きさ
	const std::size_t heapSize;

	void Up(const double val, const std::size_t nIn)
	{
		const std::size_t n = nIn +1; // TODO: 必要ならn<=heapSizeの確認

		// 末端に追加
		heap[n] = val;

		// 根に到達するまで繰り返し
		double child = val;
		for (std::size_t childIndex = n; childIndex > rootIndex;)
		{
			// 子の方が小さかったら
			const std::size_t parentIndex = childIndex / 2;
			const double parent = heap[parentIndex];
			if (child < parent)
			{
				// 親と子を交換
				heap[parentIndex] = child;
				heap[childIndex] = parent;
			}
			else
			{
				// ヒープ条件を満たしているので完了
				break;
			}

			// 次は親を見る
			childIndex = parentIndex;
		}
	}

public:
	BinaryHeap() : heap(nullptr), heapSize(0)
	{}

	BinaryHeap(const std::size_t targetSize) :
		heap(new double[targetSize + 1]),
		heapSize(targetSize)
	{}

	void Down(const double val)
	{
		// 根（最小値）より大きかったら
		double parent = heap[rootIndex];
		if (val > parent)
		{
			// 交換
			heap[rootIndex] = val;
			parent = val;

			// 末端に到達するまで繰り返し
			for (std::size_t parentIndex = rootIndex; parentIndex <= this->heapSize / 2;)
			{
				// 左の子
				std::size_t childIndex = parentIndex * 2;
				double child = heap[childIndex];

				// 右の子があって
				const std::size_t rightIndex = childIndex + 1;
				if (rightIndex <= this->heapSize)
				{
					// 右の子の方が小さかったら
					double right = heap[rightIndex]; // 右の子
					if (right < child)
					{
						// 右の子を選択
						child = right;
						childIndex = rightIndex;
					}
				}

				// 親の方が大きかったら
				if (parent > child)
				{
					// 親と子を交換
					heap[parentIndex] = child;
					heap[childIndex] = parent;
				}
				else
				{
					// ヒープ条件を満たしているので完了
					break;
				}

				// 次は子を見る
				parentIndex = childIndex;
			}
		}
	}

	void Get(const double vals[], std::size_t total)
	{
		// 対象番までのヒープを作成
		for (std::size_t i = 0; i < heapSize; i++)
		{
			Up(vals[i], i);
		}

		// 残りはヒープに追加
		for (std::size_t i = heapSize; i < total; i++)
		{
			Down(vals[i]);
		}
	}

	std::size_t HeapSize()
	{
		return heapSize;
	}

	double operator[](const size_t i)
	{
		return heap[i + 1];
	}
};
#endif

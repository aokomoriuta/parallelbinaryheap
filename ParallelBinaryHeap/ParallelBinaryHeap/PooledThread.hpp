﻿#ifndef POOLEDTHREAD_HPP_INCLUDED
#define POOLEDTHREAD_HPP_INCLUDED
#include <thread>
#include <functional>
#include <mutex>
#include <condition_variable>

class PooledThread
{
private:
	std::thread thread;

	std::mutex mtx;
	std::condition_variable cond;

	bool isKilled;

	std::function<void(void)> task;

public:
	PooledThread()
		: isKilled(false)
	{
		std::thread th([this]()
		{
			// このスレッドが起動中はずっとロックを持っておく
			std::unique_lock<std::mutex> lock(this->mtx);

			// 殺されるまで無限ループ
			for (;;)
			{
				// 起動されるまで待つ
				this->cond.wait(lock); // 待ち状態の時のみロック解除

				// 終了信号以外なら処理を実行
				if (!this->isKilled)
				{
					this->task();
				}
				else
				{
					// 関数終了
					return;
				}
			}
		});
		thread = std::move(th);
	}

	template<typename T>
	bool Start(const T t)
	{
		// ロックできたら処理開始
		std::unique_lock<std::mutex> lock(mtx, std::try_to_lock);
		if (lock)
		{
			this->task = t;

			// 処理開始を通知
			cond.notify_all();
		}

		return !!lock;
	}

	~PooledThread()
	{
		isKilled = true;
		Start([](){});

		if (thread.joinable())
		{
			thread.join();
		}
	}
};
#endif

﻿#ifndef SEMAPHORE_HPP_INCLUDED
#define SEMAPHORE_HPP_INCLUDED

#include <mutex>
#include <condition_variable>

class Semaphore
{
private:
	std::condition_variable cond;

	bool isLocked;
	std::mutex isLockedMutex;

public:
	Semaphore() : isLocked(false){};

	void Acquire()
	{
		std::unique_lock<std::mutex> lock(isLockedMutex);
		if (isLocked)
		{
			cond.wait(lock);
		}
		isLocked = true;
	}

	void Release()
	{
		std::lock_guard<std::mutex> lock(isLockedMutex);
		isLocked = false;
		cond.notify_one();
	}
};
#endif

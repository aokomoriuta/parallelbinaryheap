﻿#ifndef PARALLELBINARYHEAPN_HPP_INCLUDED
#define PARALLELBINARYHEAPN_HPP_INCLUDED
#include <new>
#include <thread>

#include "BinaryHeap.hpp"

template < std::size_t N_PARALLEL = 2>
class ParallelBinaryHeapN
{
private:
	// 並列数分の二分ヒープ
	BinaryHeap heap[N_PARALLEL];

public:
	ParallelBinaryHeapN(const std::size_t targetSize)
	{
		for (int i = 0; i < N_PARALLEL; i++)
		{
			new (heap + i) BinaryHeap(targetSize);
		}
	}

	void Get(const double vals[], std::size_t total)
	{
		std::size_t count[N_PARALLEL];
		std::size_t offset[N_PARALLEL];
		
		std::size_t sum = 0;
		for (int i = 0; i < N_PARALLEL; i++)
		{
			count[i] = std::min(total / N_PARALLEL, total - sum);
			offset[i] = sum;
			sum += count[i];
		}

		// 各スレッドでヒープを作成
		#pragma omp parallel
		{
			#pragma omp for
			for (int i = 0; i < N_PARALLEL; i++)
			{
				heap[i].Get(vals + offset[i], count[i]);
			}

			// 作成されたヒープを集約
			for (int next = 2; next <= N_PARALLEL; next *= 2)
			{
				#pragma omp for
				for (int i = 0; i < N_PARALLEL / next; i++)
				{
					for (int j = 0; j < heap[next * i].HeapSize(); j++)
					{
						heap[next * i].Down(heap[next * i + next / 2][j]);
					}
				}
			}
		}
	}

	double operator[](const size_t i)
	{
		return heap[0][i];
	}
};
#endif
